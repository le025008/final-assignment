#include "Character.h"
#include <iostream> // Include for console output
#include <algorithm> // Include for std::find_if

Character::Character(const std::string& name, int health) : name(name), health(health) {}

void Character::TakeDamage(int damage) {
    health -= damage;
    if (health < 0) health = 0;
}

void Character::AddItem(const Item& item) {
    inventory.push_back(item);

    //Debugging
    std::cout << "Inventory after adding " << item.GetName() << ":" << std::endl;
    for (const auto& invItem : inventory) {
        std::cout << "- " << invItem.GetName() << " with damage: " << invItem.GetDamage() << std::endl;
    }
}

bool Character::RemoveItem(const std::string& itemName) {
    auto it = std::find_if(inventory.begin(), inventory.end(), [&](const Item& item) {
        return item.GetName() == itemName;
        });

    if (it != inventory.end()) {
        inventory.erase(it);
        return true; // Item found and removed
    }
    return false; // Item not found
}

std::string Character::GetName() const {
    return name;
}

int Character::GetHealth() const {
    return health;
}

std::vector<Item> Character::GetInventory() const {
    return inventory;
}

void Character::ListInventory() const {
    if (inventory.empty()) {
        std::cout << "Your inventory is empty." << std::endl;
    }
    else {
        std::cout << "Inventory contains:" << std::endl;
        for (const auto& item : inventory) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }
    }
}
