#include "Item.h"
#include <iostream>

Item::Item(const std::string& name, const std::string& desc, bool isKey, std::function<void()> useAction, int damage)
    : name(name), description(desc), isKey(isKey), useAction(useAction), damage(damage) {}


void Item::Interact() const {
    std::cout << "You examine the " << name << ": " << description << std::endl;
}

void Item::Use() const {
    if (useAction) {
        useAction(); // Execute the use action if defined
    }
    else {
        std::cout << "You can't use " << name << " right now." << std::endl;
    }
}

std::string Item::GetName() const {
    return name;
}

std::string Item::GetDescription() const {
    return description;
}
int Item::GetDamage() const {
    return damage;
}
