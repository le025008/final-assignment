#include <iostream>
#include <string>
#include "Area.h"    // Make sure the Area class header is included
#include "Player.h"  // Include the Player class definition
#include <algorithm> // Necessary for std::find_if

int main() {
    Area gameWorld;

    /*   Debugging the function LoadMapFromFile
    gameWorld.AddRoom("startRoom", new Room("You are in a dimly lit room."));
    Room* testRoom = gameWorld.GetRoom("startRoom"); // Test retrieval

    if (testRoom) {
        std::cout << "Manually added room found.\n";
    }
    else {
        std::cout << "Manually added room not found.\n";
    } */
    
    // Assume LoadMapFromFile does everything:
    // - Creates rooms and adds them to the area
    // - Sets up connections between rooms
    // - Optionally, places items in rooms

    gameWorld.LoadMapFromFile("game_map.txt");

    // Create a Player and set the starting room
    Player player("Kieu", 100, 0, 5);
    Room* currentRoom = gameWorld.GetRoom("startRoom");
    if (currentRoom) {
        player.SetLocation(currentRoom);
    }
    else {
        std::cerr << "Starting room not found. Check your game map." << std::endl;
        return -1; // Exit if the starting room doesn't exist
    }

    bool running = true;
    while (running) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (const Item& item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }
        //Combat system
        Room* currentRoom = player.GetLocation();
        Enemy* enemy = currentRoom->GetEnemy();

        if (enemy && enemy->IsAlive()) {
            while (enemy->IsAlive()) {
                std::cout << "You are in combat with a " << enemy->GetName() << "!" << std::endl;
                std::cout << "Choose your action: \n1. Attack\n2. Flee" << std::endl;
                int combatChoice;
                std::cin >> combatChoice;

                if (combatChoice == 1) {
                    int damageDealt = player.CalculateTotalDamage();
                    enemy->TakeDamage(damageDealt);
                    std::cout << "You attack the " << enemy->GetName() << " for " << damageDealt << " damage." << std::endl;
                    if (!enemy->IsAlive()) {
                        std::cout << "You have defeated the " << enemy->GetName() << "!" << std::endl;
                        break; // Exit combat loop
                    }
                }
                else if (combatChoice == 2) {
                    std::cout << "You flee from the " << enemy->GetName() << "." << std::endl;
                    // Implement fleeing logic
                    break; // Exit combat loop
                }

                // Enemy's turn to attack
                player.TakeDamage(enemy->GetAttackPower());
                std::cout << "The " << enemy->GetName() << " hits you for " << enemy->GetAttackPower() << " damage." << std::endl;
                if (!player.IsAlive()) {
                    std::cout << "You have been killed by the " << enemy->GetName() << "!" << std::endl;
                    // Handle player death, possibly ending the game
                    running = false;
                    break; // Exit combat loop
                }
            }
        }

        // Display available exits
        std::cout << "Exits available: ";
        for (const auto& exit : player.GetLocation()->GetExits()) {
            std::cout << exit.first << " ";
        }
        std::cout << std::endl;

        std::cout << "Choose an action:\n1. Look around\n2. Pick up an item\n3. Move to another room\n4. View inventory\n5. Use an item\n6. Quit\n";
        int choice;
        std::cin >> choice;

        switch (choice) {
        case 1: {
            // Look around
            std::cout << "You look around the room." << std::endl;
            break;
        }
        case 2: {
            // Pick up an item
            std::cout << "Enter the name of the item you want to pick up: ";
            std::string itemName;
            std::cin >> itemName;

            const auto& items = player.GetLocation()->GetItems();
            auto it = std::find_if(items.begin(), items.end(), [&itemName](const Item& item) {
                return item.GetName() == itemName;
                });

            if (it != items.end()) {
                player.AddItem(*it); // Add item to player's inventory
                player.GetLocation()->RemoveItem(itemName); // Remove item from the room
                std::cout << itemName << " picked up." << std::endl;
            }
            else {
                std::cout << "Item not found in the room." << std::endl;
            }
            break;
        }
        case 3: {
            std::cout << "Enter the direction (e.g., north, south, east, west): ";
            std::string direction;
            std::cin >> direction;

            Room* nextRoom = player.GetLocation()->GetExit(direction);
            if (nextRoom) {
                if (nextRoom->GetIsLocked()) {
                    if (player.HasItem("key")) { // Use the actual key's name
                        if (nextRoom->Unlock("key")) { // Unlock the room using the key's name
                            std::cout << "You unlock the room and move to the next room." << std::endl;
                            player.SetLocation(nextRoom);
                        }
                        else {
                            std::cout << "You have the key, but it doesn't fit the lock." << std::endl;
                        }
                    }
                    else {
                        std::cout << "The room is locked. You need the key to unlock it." << std::endl;
                    }
                }
                else {
                    player.SetLocation(nextRoom);
                    std::cout << "You move to the next room." << std::endl;
                }
            }
            else {
                std::cout << "You can't go that way." << std::endl;
            }
            break;
        }
        case 4:
            // View inventory
            player.ListInventory();
            break;
        case 5: {
            std::cout << "Enter the name of the item you want to use: ";
            std::string itemName;
            std::cin >> itemName;

            bool itemUsed = false;
            for (auto& item : player.GetInventory()) {
                if (item.GetName() == itemName) {
                    item.Use(); // Assuming Item has a Use method implemented
                    itemUsed = true;
                    break;
                }
            }

            if (!itemUsed) {
                std::cout << "Item not found in inventory or cannot be used right now." << std::endl;
            }
            break;
        }
        case 6:
            // Quit the game
            std::cout << "Quitting the game. Goodbye!" << std::endl;
            running = false;
            break;
        default:
            std::cout << "Invalid choice. Please try again." << std::endl;
            break;
        }
    }

    return 0;
}
