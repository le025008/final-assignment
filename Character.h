#pragma once
#ifndef CHARACTER_H
#define CHARACTER_H
#include <string>
#include <vector>
#include "Item.h"

class Character {
private:
    std::string name;
    int health;


public:
    Character(const std::string& name, int health);
    virtual ~Character() = default; // Ensure proper deletion of derived classes

    void TakeDamage(int damage);
    void AddItem(const Item& item);
    bool RemoveItem(const std::string& itemName); // Changed to use item name
    std::string GetName() const;
    int GetHealth() const;
    std::vector<Item> GetInventory() const;
    void ListInventory() const; // New method to list items in inventory
    std::vector<Item> inventory;
};

#endif // CHARACTER_H
