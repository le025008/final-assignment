#include "Player.h"
#include <algorithm>

Player::Player(const std::string& name, int health, int attackPower, int baseAttackPower)
    : Character(name, health), location(nullptr), attackPower(attackPower), baseAttackPower(baseAttackPower){}

int Player::GetAttackPower() const {
    return attackPower;
}
int Player::BaseAttackPower() const {
    return baseAttackPower;
}

void Player::SetLocation(Room* newLocation) {
    location = newLocation;
}

void Player::Attack(Enemy& enemy) {
    std::cout << "You attack the " << enemy.GetName() << " for " << attackPower << " damage." << std::endl;
    enemy.TakeDamage(attackPower);
    if (!enemy.IsAlive()) {
        std::cout << "The " << enemy.GetName() << " is defeated!" << std::endl;
    }
}

Room* Player::GetLocation() const {
    return location;
}
bool Player::HasItem(const std::string& itemName) const {
    // Use std::find_if with a lambda function to search for the item by name
    auto it = std::find_if(inventory.begin(), inventory.end(), [&](const Item& item) {
        return item.GetName() == itemName;
        });

    // If the item is found, return true. Otherwise, return false.
    return it != inventory.end();
}
bool Player::IsAlive() const {
    return GetHealth() > 0;
}
int Player::CalculateTotalDamage() const {
    int totalDamage = baseAttackPower; // Start with the player's base attack power
    for (const auto& item : inventory) {
        std::cout << "Adding item damage: " << item.GetDamage() << " from item: " << item.GetName() << std::endl;
        totalDamage += item.GetDamage(); // Add the damage of each item
    }
    return totalDamage;
}