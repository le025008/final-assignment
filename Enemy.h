#pragma once
#ifndef ENEMY_H
#define ENEMY_H

#include <string>

class Enemy {
private:
    std::string name;
    int health;
    int attackPower;

public:
    Enemy(const std::string& name, int health, int attackPower);
    std::string GetName() const;
    int GetHealth() const;
    int GetAttackPower() const;
    void TakeDamage(int amount);
    bool IsAlive() const;
};

#endif // ENEMY_H