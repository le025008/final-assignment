#include "Area.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <string>


// Trim function definition
std::string trim(const std::string& str) {
    size_t first = str.find_first_not_of(' ');
    if (first == std::string::npos)
        return "";
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

Area::Area() {}
Area::~Area() {
    // Deallocate memory to prevent memory leaks.
    for (auto& pair : rooms) {
        delete pair.second;
    }
}

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) const {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return it->second;
    }
    return nullptr;
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);

    if (room1 && room2) {
        room1->AddExit(direction, room2);
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    std::string line;

    while (getline(file, line)) {
        std::istringstream lineStream(line);
        std::string prefix;
        getline(lineStream, prefix, ':');
        prefix = trim(prefix); // Trim the prefix

        if (prefix == "Room") {
            std::string name, desc, isLockedStr, keyName;
            bool isLocked = false;

            getline(lineStream, name, ',');
            getline(lineStream, desc, ',');
            getline(lineStream, isLockedStr, ',');
            getline(lineStream, keyName); // May be empty if not a locked room

            name = trim(name);
            desc = trim(desc);
            keyName = trim(keyName);
            isLocked = !isLockedStr.empty() && trim(isLockedStr) == "true"; // Check if the locked parameter is true

            Room* room = new Room(desc, isLocked, keyName);
            AddRoom(name, room);
        }
        else if (prefix == "Item") {
            std::string roomName, itemName, itemDesc, isKeyStr, damageStr;
            bool isKey = false;
            getline(lineStream, roomName, ',');
            getline(lineStream, itemName, ',');
            getline(lineStream, itemDesc, ',');
            getline(lineStream, isKeyStr, ','); // May be empty if not a key
            getline(lineStream, damageStr);

            //Debugging item's damage
            damageStr = trim(damageStr);
            std::cout << "Parsed damage string: '" << damageStr << "'" << std::endl;
            for (char c : damageStr) {
                std::cout << static_cast<int>(c) << " "; // Print ASCII values
            }
            std::cout << std::endl;
    
            int damage = 0;
            try {
                damage = std::stoi(damageStr); // Convert string to integer for damage
                std::cout << "Converted damage to integer: " << damage << std::endl;
            }
            catch (const std::invalid_argument& e) {
                std::cerr << "Error: Invalid damage value for item " << itemName << ": " << e.what() << std::endl;
            }

           
            roomName = trim(roomName); // Trim the room name
            itemName = trim(itemName); // Trim the item name
            itemDesc = trim(itemDesc); // Trim the item description
            isKey = !isKeyStr.empty() && trim(isKeyStr) == "true"; // Check if the key parameter is true

            Item newItem(itemName, itemDesc, isKey, nullptr, damage);

            Room* room = GetRoom(roomName);
            if (room) {
                room->AddItem(Item(itemName, itemDesc, isKey, nullptr, damage));
            }
        }
        else if (prefix == "Connect") {
            std::string room1Name, room2Name, direction;
            getline(lineStream, room1Name, ',');
            getline(lineStream, room2Name, ',');
            getline(lineStream, direction);
            room1Name = trim(room1Name); // Trim the first room name
            room2Name = trim(room2Name); // Trim the second room name
            direction = trim(direction); // Trim the direction
            ConnectRooms(room1Name, room2Name, direction);

        }
        else if (prefix == "Enemy") {
            std::string roomName, enemyName, healthStr, attackStr;
            getline(lineStream, roomName, ',');
            getline(lineStream, enemyName, ',');
            getline(lineStream, healthStr, ',');
            getline(lineStream, attackStr);

            int health = std::stoi(trim(healthStr));
            int attackPower = std::stoi(trim(attackStr));
            roomName = trim(roomName);
            enemyName = trim(enemyName);
         
            Room* room = GetRoom(roomName);
            if (room) {
                room->AddEnemy(Enemy(enemyName, health, attackPower));
            }
        }
    }
}


