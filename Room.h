#pragma once
#ifndef ROOM_H
#define ROOM_H
#include "Enemy.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "Item.h"

class Room {
private:
    std::string description;
    bool isLocked;
    std::string keyName;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;
    std::vector<Enemy> enemies; // To hold enemies in the room
    Enemy* enemy; //Pointer to enemy in the room
public:
   Room(const std::string& desc, bool isLocked = false, const std::string& keyName = "");
   Enemy* GetEnemy() const;

    bool Unlock(const std::string& itemName) {
        if (isLocked && itemName == keyName) {
            isLocked = false;
            return true;
        }
        return false;
    }
    bool GetIsLocked() const { return isLocked; }
    void AddEnemy(const Enemy& enemy);
    void AddItem(const Item& item);
    void RemoveItem(const std::string& itemName);   
    void AddExit(const std::string& direction, Room* room);
    const std::vector<Item>& GetItems() const;
    const std::map<std::string, Room*>& GetExits() const;
    std::string GetDescription() const;
    Room* GetExit(const std::string& direction) const; // Declare the method
};

#endif // ROOM_H
