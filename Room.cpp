#include "Room.h"

Room::Room(const std::string& desc, bool isLocked, const std::string& keyName)
    : description(desc), isLocked(isLocked), keyName(keyName), enemy(nullptr) {}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const std::string& itemName) {
    auto it = std::find_if(items.begin(), items.end(), [&](const Item& item) {
        return item.GetName() == itemName;
        });

    if (it != items.end()) {
        items.erase(it); // Remove item from the room
    }
}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

const std::vector<Item>& Room::GetItems() const {
    return items;
}

const std::map<std::string, Room*>& Room::GetExits() const {
    return exits;
}

std::string Room::GetDescription() const {
    return description;
}
Room* Room::GetExit(const std::string& direction) const {
    auto it = exits.find(direction); // Attempt to find an exit in the specified direction
    if (it != exits.end()) {
        return it->second; // If found, return the Room pointer
    }
    else {
        return nullptr; // If not found, return nullptr
    }
}
void Room::AddEnemy(const Enemy& enemy) {
    enemies.push_back(enemy);
}

Enemy* Room::GetEnemy() const {
    if (!enemies.empty()) {
        return const_cast<Enemy*>(&enemies.front());
    }
    return nullptr; // No enemy in this room
}