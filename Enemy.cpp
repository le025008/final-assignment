#include "Enemy.h"

Enemy::Enemy(const std::string& name, int health, int attackPower)
    : name(name), health(health), attackPower(attackPower) {}

int Enemy::GetHealth() const {
    return health;
}

int Enemy::GetAttackPower() const {
    return attackPower;
}

void Enemy::TakeDamage(int amount) {
    health -= amount;
    if (health < 0) {
        health = 0;
    }
}

bool Enemy::IsAlive() const {
    return health > 0;
}
std::string Enemy::GetName() const {
    return name;
}