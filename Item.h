#pragma once
#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <functional> // For std::function

class Item {
private:
    std::string name;
    std::string description;
    std::function<void()> useAction; // Function to define what happens when the item is used
    bool isKey;
    int damage{ 0 };

public:
    Item(const std::string& name, const std::string& desc, bool isKey = false, std::function<void()> useAction = nullptr, int damage = 0);
    bool GetIsKey() const { return isKey; }
    void Interact() const;
    void Use() const; // Use the item
    std::string GetName() const;
    std::string GetDescription() const;
    int GetDamage() const;
};

#endif // ITEM_H
