#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include "Character.h"
#include "Room.h" // Make sure class is defined
#include <string>

class Player : public Character {
private:
    Room* location; // Current location of the player
    int attackPower; //Combat
    int baseAttackPower;

public:
    Player(const std::string& name, int health, int attackPower, int baseAttackPower);
    int BaseAttackPower() const;
    void SetLocation(Room* newLocation);
    Room* GetLocation() const;
    bool HasItem(const std::string& itemName) const; // Declare HasItem method to use keys
    // New combat system
    int GetAttackPower() const;
    void Attack(Enemy& enemy);
    bool IsAlive() const;
    int CalculateTotalDamage() const;
};

#endif // PLAYER_H
